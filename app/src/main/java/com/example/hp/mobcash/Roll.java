package com.example.hp.mobcash;

public class Roll {
    String roll;

    public Roll(String roll) {
        this.roll=roll;
    }
    public Roll(){
    }

    public String getRoll() {
        return roll;
    }

    public void setRoll(String roll) {
        this.roll = roll;
    }
}
