package com.example.hp.mobcash;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;

public class RegisterActivity extends AppCompatActivity {
    EditText et_roll, et_pass, et_nm, et_fname, et_mname, et_email;

    Spinner et_cat, et_gen, et_dept, et_course, et_sem, et_stat;
    String email, pass, roll, name, fname, mname, course, cat, gen, dept, sem, stat,fid,fstatus;
    Button save;
    FirebaseAuth auth;
    ProgressDialog progressDialog;
    FirebaseFirestore fb;
    DocumentReference DocumentRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        auth = FirebaseAuth.getInstance();
        et_email=findViewById(R.id.et_email);
        et_pass=findViewById(R.id.et_pass);
        et_roll = findViewById(R.id.et_roll);
        et_nm = findViewById(R.id.et_name);
        et_fname = findViewById(R.id.et_fname);
        et_mname = findViewById(R.id.et_mname);
        et_gen=findViewById(R.id.et_gender);
        et_cat=findViewById(R.id.et_category);
        et_dept=findViewById(R.id.et_dept);
        et_course=findViewById(R.id.et_course);
        et_sem=findViewById(R.id.et_sem);
        et_stat=findViewById(R.id.et_status);
        save = findViewById(R.id.btn_save);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("wait....");

        et_gen = (Spinner) this.findViewById(R.id.et_gender);
        ArrayAdapter<CharSequence> dataAdapter =ArrayAdapter.createFromResource(this, R.array.Gender, R.layout.simple_spinner_item);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        et_gen.setAdapter(dataAdapter);

        et_cat= (Spinner) this.findViewById(R.id.et_category);
        ArrayAdapter<CharSequence> dataAdapter1 = ArrayAdapter.createFromResource(this, R.array.Category, R.layout.simple_spinner_item);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_list_item_1);
        et_cat.setAdapter(dataAdapter1);

        et_dept = (Spinner) this.findViewById(R.id.et_dept);
        ArrayAdapter<CharSequence> dataAdapter2 = ArrayAdapter.createFromResource(this, R.array.DEPARTMENT, R.layout.simple_spinner_item);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_list_item_1);
        et_dept.setAdapter(dataAdapter2);

        et_course = (Spinner) this.findViewById(R.id.et_course);
        ArrayAdapter<CharSequence> dataAdapter3 = ArrayAdapter.createFromResource(this, R.array.COURSE, R.layout.simple_spinner_item);
        dataAdapter3.setDropDownViewResource(android.R.layout.simple_list_item_1);
        et_course.setAdapter(dataAdapter3);

        et_sem = (Spinner) this.findViewById(R.id.et_sem);
        ArrayAdapter<CharSequence> dataAdapter4 = ArrayAdapter.createFromResource(this, R.array.SEMESTER, R.layout.simple_spinner_item);
        dataAdapter4.setDropDownViewResource(android.R.layout.simple_list_item_1);
        et_sem.setAdapter(dataAdapter4);

        et_stat = (Spinner) this.findViewById(R.id.et_status);
        ArrayAdapter<CharSequence> dataAdapter5 = ArrayAdapter.createFromResource(this, R.array.STATUS, R.layout.simple_spinner_item);
        dataAdapter5.setDropDownViewResource(android.R.layout.simple_list_item_1);
        et_stat.setAdapter(dataAdapter5);



        save = findViewById(R.id.btn_save);
        auth=FirebaseAuth.getInstance();
        fb = FirebaseFirestore.getInstance();
        final DocumentReference DocumentRef;
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                email = et_email.getText().toString();
                pass = et_pass.getText().toString();
                roll = et_roll.getText().toString();
                name = et_nm.getText().toString();
                fname = et_fname.getText().toString();
                mname = et_mname.getText().toString();
                gen=et_gen.getSelectedItem().toString();
                cat=et_cat.getSelectedItem().toString();
                dept=et_dept.getSelectedItem().toString();
                course=et_course.getSelectedItem().toString();
                sem=et_sem.getSelectedItem().toString();
                stat=et_stat.getSelectedItem().toString();
                fstatus="DUE";

                if(roll.equals("")||name.equals("")||fname.equals("")||mname.equals("")||email.equals("")||pass.equals("")){
                   progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"All Fields Required",Toast.LENGTH_LONG).show();
                }
                else {
                    createUser(email, pass);
                }
            }
        });

    }

    private void createUser(String email, String pass) {
        auth.createUserWithEmailAndPassword(email, pass)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            storeData();
                        }
                    }
                });
    }




    private void storeData() {

        fid=course.substring(0,1).concat(gen.substring(0,1)).concat(cat.substring(0,2)).concat(sem.substring(0,1)).concat(dept.substring(0,2));

        CollectionReference Email = fb.collection("EMAIL");
            Roll r=new Roll(roll);
        fb.collection("EMAIL").document(email).set(r).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    addRollData();
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Detail Not Recorded", Toast.LENGTH_LONG).show();
                }
            }
        });



    }

    private void addRollData() {
        CollectionReference ROLL = fb.collection("Roll No.");
        UserDataR user = new UserDataR(name,fname, mname, gen, cat, course, sem, stat,fid,dept,fstatus);
        fb.collection("Roll No.").document(roll).set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Detail Recorded", Toast.LENGTH_LONG).show();
                    clearlayout();
                    startActivity(new Intent(getApplicationContext(),HomeActivity.class));
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Detail Not Recorded", Toast.LENGTH_LONG).show();
                }
            }
        });

    }


    private void clearlayout() {
        et_stat.setSelection(0);
        et_sem.setSelection(0);
        et_cat.setSelection(0);
        et_course.setSelection(0);
        et_gen.setSelection(0);
        et_dept.setSelection(0);
        et_nm.setText("");
        et_roll.setText("");
        et_fname.setText("");
        et_mname.setText("");
        et_email.setText("");
        et_pass.setText("");

    }
}



