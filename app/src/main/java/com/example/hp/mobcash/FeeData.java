package com.example.hp.mobcash;

public class FeeData {
    String course,dept,sem,gen,cat,period;
    Double sec,lib,swf,med,uni,tui,fine,con,total;

    public FeeData(String course, String dept, String sem, String gen, String cat, String period, Double sec, Double lib, Double swf, Double med, Double uni, Double tui, Double fine, Double con, Double total) {
        this.course = course;
        this.dept = dept;
        this.sem = sem;
        this.gen = gen;
        this.cat = cat;
        this.period = period;
        this.sec = sec;
        this.lib = lib;
        this.swf = swf;
        this.med = med;
        this.uni = uni;
        this.tui = tui;
        this.fine = fine;
        this.con = con;
        this.total = total;
    }

    public FeeData(){

    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getSem() {
        return sem;
    }

    public void setSem(String sem) {
        this.sem = sem;
    }

    public String getGen() {
        return gen;
    }

    public void setGen(String gen) {
        this.gen = gen;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public Double getSec() {
        return sec;
    }

    public void setSec(Double sec) {
        this.sec = sec;
    }

    public Double getLib() {
        return lib;
    }

    public void setLib(Double lib) {
        this.lib = lib;
    }

    public Double getSwf() {
        return swf;
    }

    public void setSwf(Double swf) {
        this.swf = swf;
    }

    public Double getMed() {
        return med;
    }

    public void setMed(Double med) {
        this.med = med;
    }

    public Double getUni() {
        return uni;
    }

    public void setUni(Double uni) {
        this.uni = uni;
    }

    public Double getTui() {
        return tui;
    }

    public void setTui(Double tui) {
        this.tui = tui;
    }

    public Double getFine() {
        return fine;
    }

    public void setFine(Double fine) {
        this.fine = fine;
    }

    public Double getCon() {
        return con;
    }

    public void setCon(Double con) {
        this.con = con;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}
