package com.example.hp.mobcash;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class StudentDetails extends Fragment {
    EditText et_roll, et_nm, et_fname, et_mname;

    EditText et_cat, et_gen, et_dept, et_course, et_sem, et_stat;
    String email, fid,roll;
ProgressDialog progressDialog;
    FirebaseAuth auth;

    FirebaseFirestore fb;
    DocumentReference DocumentRef;

    ImageButton btn_info;
    ImageButton btn_nav;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.std_details, container, false);
        et_roll = view.findViewById(R.id.et_roll);
        et_nm = view.findViewById(R.id.et_name);
        et_fname = view.findViewById(R.id.et_fname);
        et_mname = view.findViewById(R.id.et_mname);
        et_gen = view.findViewById(R.id.et_gender);
        et_cat = view.findViewById(R.id.et_category);
        et_dept = view.findViewById(R.id.et_dept);
        et_course = view.findViewById(R.id.et_course);
        et_sem = view.findViewById(R.id.et_sem);
        et_stat = view.findViewById(R.id.et_status);
        auth = FirebaseAuth.getInstance();
        email = auth.getCurrentUser().getEmail();
        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage("Wait...");
        progressDialog.show();
        fb = FirebaseFirestore.getInstance();
        final DocumentReference DocumentRef;
        setData();
        return view;

    }

    private void setData() {
        fb.collection("EMAIL").document(email).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Roll r = documentSnapshot.toObject(Roll.class);
                roll=r.getRoll().toString();
                et_roll.setText(roll);
                getRollData();
                progressDialog.dismiss();
            }
        });


    }

    private void getRollData() {
        fb.collection("Roll No.").document(roll).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                UserDataR userData = documentSnapshot.toObject(UserDataR.class);
                et_cat.setText(userData.getCat());
                et_course.setText(userData.getCourse());
                et_dept.setText(userData.getDept());
                et_nm.setText(userData.getName());
                et_gen.setText(userData.getGen());
                et_fname.setText(userData.getFname());
                et_mname.setText(userData.getMname());
                et_sem.setText(userData.getSem());
                et_stat.setText(userData.getStat());
                fid = userData.getFid().toString();

            }
        });





    }
}

