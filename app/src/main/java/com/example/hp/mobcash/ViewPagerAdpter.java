package com.example.hp.mobcash;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class ViewPagerAdpter extends FragmentStatePagerAdapter {
    int tabcount;

    public ViewPagerAdpter(FragmentManager fm, int tabcount) {
        super(fm);
        this.tabcount = tabcount;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0: {
                StudentDetails oneFragment = new StudentDetails();
                return oneFragment;
            }

            case 1: {
                FeePayment twoFragment = new FeePayment();
                return twoFragment;

            }

            default:
                return null;
        }
    }

    @Override
    public int getCount() {

        return tabcount;
    }
}
