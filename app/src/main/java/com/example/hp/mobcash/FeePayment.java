package com.example.hp.mobcash;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

public class FeePayment extends Fragment {
String roll,email,fid,fstatus;
double total;
    EditText et_fstatus,et_period,et_sec,et_swf,et_lib,et_med,et_uni,et_tui,et_fine,et_con,et_total;
    Button btn_pay;
    ProgressDialog progressDialog;
    FirebaseAuth auth;
    FirebaseFirestore fb;
    DocumentReference DocumentRef;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fee_pay, container, false);
        et_fstatus=view.findViewById(R.id.et_feestatus);
        et_period=view.findViewById(R.id.et_period);
        et_sec=view.findViewById(R.id.et_security);
        et_swf=view.findViewById(R.id.et_fund);
        et_lib=view.findViewById(R.id.et_lib_fee);
        et_med=view.findViewById(R.id.et_medical);
        et_uni=view.findViewById(R.id.et_uni_fee);
        et_tui=view.findViewById(R.id.et_tuition);
        et_con=view.findViewById(R.id.et_concession);
        et_fine=view.findViewById(R.id.et_fine);
        et_total=view.findViewById(R.id.et_total);
       btn_pay=view.findViewById(R.id.btn_pay);
        progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage("Wait...");
        progressDialog.show();
        auth = FirebaseAuth.getInstance();
        email = auth.getCurrentUser().getEmail();

        fb = FirebaseFirestore.getInstance();
        final DocumentReference DocumentRef;
        fb.collection("EMAIL").document(email).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Roll r = documentSnapshot.toObject(Roll.class);
                roll=r.getRoll().toString();
                GETFID();


    }

});
        progressDialog.dismiss();
        btn_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                proceedToPay();
            }
        });
        return view;    }

    private void GETFID() {
        fb.collection("Roll No.").document(roll).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                UserDataR userData = documentSnapshot.toObject(UserDataR.class);
                fid = userData.getFid().toString();
                fstatus=userData.getFstatus().toString();
              getFeeAmount();

            }
        });



    }

    private void proceedToPay() {
        Toast.makeText(getActivity(),"Payment Gateway Not Found",Toast.LENGTH_LONG).show();
    }


    private void getFeeAmount() {
        fb.collection("FID").document(fid).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                FeeData feeData = documentSnapshot.toObject(FeeData.class);
                et_fstatus.setText(fstatus);
                if(fstatus.equals("Due")){btn_pay.setEnabled(true);}
                et_con.setText(feeData.getCon().toString());
                et_fine.setText(feeData.getFine().toString());
                et_sec.setText(feeData.getSec().toString());
                et_swf.setText(feeData.getSwf().toString());
                et_med.setText(feeData.getMed().toString());
                et_lib.setText(feeData.getLib().toString());
                et_period.setText(feeData.getPeriod());
                et_uni.setText(feeData.getUni().toString());
                et_tui.setText(feeData.getTui().toString());
                total=feeData.getTotal();
                et_total.setText(Double.toString(total));

            }
        });
    }
}
