package com.example.hp.mobcash;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {
    EditText et_user, et_pass;
    Button btn_log, btn_register;
    FirebaseAuth auth;
    ProgressDialog p;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et_user = findViewById(R.id.et_username);
        et_pass = findViewById(R.id.et_password);
        btn_log = findViewById(R.id.btn_login);
        btn_register = findViewById(R.id.btn_register);
        auth = FirebaseAuth.getInstance();
        p = new ProgressDialog(this);
        p.setMessage("Wait...");

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            }
        });
        btn_log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }

            private void login() {

                p.show();
                final String email = et_user.getText().toString().trim();
                String pass = et_pass.getText().toString().trim();
                if (email.equals("") || pass.equals("")) {
                    p.dismiss();
                    Toast.makeText(getApplicationContext(), "All fields required..", Toast.LENGTH_LONG).show();
                } else {
                    auth.signInWithEmailAndPassword(email, pass)
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    p.dismiss();
                                    Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            })
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        p.dismiss();
                                        Toast.makeText(MainActivity.this, "user loged in", Toast.LENGTH_SHORT).show();
                                        startActivity(new Intent(getApplicationContext(), HomeActivity.class).putExtra("user", email));

                                    }

                                }
                            });
                }
            }
        });
    }
}

