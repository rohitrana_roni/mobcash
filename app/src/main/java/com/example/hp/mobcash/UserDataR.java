package com.example.hp.mobcash;

public class UserDataR {
    private String name, fname, mname, gen, cat, course, sem, stat, fid, dept, fstatus;

    public UserDataR( String name, String fname, String mname, String gen, String cat, String course, String sem, String stat, String fid, String dept, String fstatus) {

        this.name = name;
        this.fname = fname;
        this.mname = mname;
        this.gen = gen;
        this.dept = dept;
        this.cat = cat;
        this.course = course;
        this.sem = sem;
        this.stat = stat;
        this.fid = fid;
        this.fstatus = fstatus;
    }
    public UserDataR() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getGen() {
        return gen;
    }

    public void setGen(String gen) {
        this.gen = gen;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getSem() {
        return sem;
    }

    public void setSem(String sem) {
        this.sem = sem;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }


    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }


    public String getFstatus() {
        return fstatus;
    }

    public void setFstatus(String fstatus) {
        this.fstatus = fstatus;
    }
}
